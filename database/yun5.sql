/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3307
 Source Schema         : yun5

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 13/04/2023 16:44:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_actorhis
-- ----------------------------
DROP TABLE IF EXISTS `t_actorhis`;
CREATE TABLE `t_actorhis`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ACTORID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AGENTID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AGENTNAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `TYPE` int(11) NULL DEFAULT NULL,
  `PROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `ATTITUDE` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NODEHIS_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SIGNATURE` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `RECEIVERINFO` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ACTIONTIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_actorrt
-- ----------------------------
DROP TABLE IF EXISTS `t_actorrt`;
CREATE TABLE `t_actorrt`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ACTORID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ISPROCESSED` bit(1) NULL DEFAULT NULL,
  `TYPE` int(11) NULL DEFAULT NULL,
  `NODERT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DEADLINE` datetime(0) NULL DEFAULT NULL,
  `PENDING` bit(1) NULL DEFAULT NULL,
  `ISREAD` int(11) NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPROVAL_POSITION` int(11) NULL DEFAULT NULL,
  `REMINDER_TIMES` int(11) NULL DEFAULT NULL,
  `BAK` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_checkin
-- ----------------------------
DROP TABLE IF EXISTS `t_checkin`;
CREATE TABLE `t_checkin`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `WIDGET_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATION_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOMAIN_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `USER_NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CHECKIN_TIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_circulator
-- ----------------------------
DROP TABLE IF EXISTS `t_circulator`;
CREATE TABLE `t_circulator`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `USERID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NODERT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CCTIME` datetime(0) NULL DEFAULT NULL,
  `READTIME` datetime(0) NULL DEFAULT NULL,
  `DEADLINE` datetime(0) NULL DEFAULT NULL,
  `ISREAD` int(11) NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SUMMARY` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `VERSION` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_coactorrt
-- ----------------------------
DROP TABLE IF EXISTS `t_coactorrt`;
CREATE TABLE `t_coactorrt`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `USERID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NODERT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CREATE_TIME` datetime(0) NULL DEFAULT NULL,
  `READTIME` datetime(0) NULL DEFAULT NULL,
  `DEADLINE` datetime(0) NULL DEFAULT NULL,
  `ISREAD` int(11) NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `VERSION` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `FLAG` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATION_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOMAIN_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `USER_NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `COMMENTS` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `UNLIKE_NUM` int(11) NULL DEFAULT NULL,
  `LIKE_NUM` int(11) NULL DEFAULT NULL,
  `CREATE_DATE` datetime(0) NULL DEFAULT NULL,
  `PARENT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_counter
-- ----------------------------
DROP TABLE IF EXISTS `t_counter`;
CREATE TABLE `t_counter`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `COUNTER` int(11) NULL DEFAULT NULL,
  `NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_document
-- ----------------------------
DROP TABLE IF EXISTS `t_document`;
CREATE TABLE `t_document`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `SORTID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATELABEL` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `INITIATOR` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `LASTFLOWOPERATION` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `PARENT` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATE` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `MAPPINGID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_flow_proxy
-- ----------------------------
DROP TABLE IF EXISTS `t_flow_proxy`;
CREATE TABLE `t_flow_proxy`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `FLOWNAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATE` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AGENTS` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `AGENTSNAME` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `OWNER` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `VERSION` int(11) NULL DEFAULT NULL,
  `PROXYMODE` int(11) NULL DEFAULT NULL,
  `STARTPROXYTIME` datetime(0) NULL DEFAULT NULL,
  `ENDPROXYTIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_flowhistory
-- ----------------------------
DROP TABLE IF EXISTS `t_flowhistory`;
CREATE TABLE `t_flowhistory`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `FLOWSTATERT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NODEHIS_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STARTNODENAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ENDNODENAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ATTITUDE` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `PROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `FLOWOPERATION` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STARTNODEID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ENDNODEID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AGENTNAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AGENTID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SIGNATURE` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ACTORID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RECEIVERINFO` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ACTIONTIME` datetime(0) NULL DEFAULT NULL,
  `FLOWNAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `INITIATOR` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `INITIATORID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SUMMARY` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `FIRSTPROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `STATELABEL` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_flowstatert
-- ----------------------------
DROP TABLE IF EXISTS `t_flowstatert`;
CREATE TABLE `t_flowstatert`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `DOCID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATE` int(11) NULL DEFAULT NULL,
  `PARENT` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWNAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWXML` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `LASTMODIFIERID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SUBFLOWNODEID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `COMPLETE` int(11) NULL DEFAULT NULL,
  `CALLBACK` int(11) NULL DEFAULT NULL,
  `TOKEN` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATELABEL` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `INITIATOR` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `LASTFLOWOPERATION` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `SUB_POSITION` int(11) NULL DEFAULT NULL,
  `ISARCHIVED` int(11) NULL DEFAULT NULL,
  `ISTERMINATED` bit(1) NULL DEFAULT NULL,
  `PREV_AUDIT_NODE` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `PREV_AUDIT_USER` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `WORKFLOW_TYPE` int(11) NULL DEFAULT NULL,
  `ACTIONTIME` datetime(0) NULL DEFAULT NULL,
  `SUMMARY` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `INITIATORID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FIRSTPROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_nodert
-- ----------------------------
DROP TABLE IF EXISTS `t_nodert`;
CREATE TABLE `t_nodert`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NODEID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOCID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NOTIFIABLE` bit(1) NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATELABEL` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWOPTION` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SPLITTOKEN` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `PASSCONDITION` int(11) NULL DEFAULT NULL,
  `PARENTNODERTID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DEADLINE` datetime(0) NULL DEFAULT NULL,
  `ORDERLY` bit(1) NULL DEFAULT NULL,
  `APPROVAL_POSITION` int(11) NULL DEFAULT NULL,
  `STATE` int(11) NULL DEFAULT NULL,
  `LASTPROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `REMINDER_TIMES` int(11) NULL DEFAULT NULL,
  `ACTIONTIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_relationhis
-- ----------------------------
DROP TABLE IF EXISTS `t_relationhis`;
CREATE TABLE `t_relationhis`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ACTIONTIME` datetime(0) NULL DEFAULT NULL,
  `PROCESSTIME` datetime(0) NULL DEFAULT NULL,
  `STARTNODENAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWNAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOCID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ENDNODEID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ENDNODENAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STARTNODEID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ISPASSED` bit(1) NULL DEFAULT NULL,
  `ATTITUDE` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITOR` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOWOPERATION` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `REMINDERCOUNT` int(11) NULL DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shortmessage_received
-- ----------------------------
DROP TABLE IF EXISTS `t_shortmessage_received`;
CREATE TABLE `t_shortmessage_received`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `CONTENT` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `SENDER` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RECEIVER` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RECEIVEDATE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATUS` int(11) NULL DEFAULT NULL,
  `PARENT` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOCID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_shortmessage_submit
-- ----------------------------
DROP TABLE IF EXISTS `t_shortmessage_submit`;
CREATE TABLE `t_shortmessage_submit`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `CONTENTTYPE` int(11) NULL DEFAULT NULL,
  `TITLE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CONTENT` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SENDDATE` datetime(0) NULL DEFAULT NULL,
  `REPLYCODE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SENDER` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RECEIVER` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `SUBMISSION` bit(1) NULL DEFAULT NULL,
  `ISFAILURE` bit(1) NULL DEFAULT NULL,
  `ISREPLY` bit(1) NULL DEFAULT NULL,
  `ISTRASH` bit(1) NULL DEFAULT NULL,
  `ISDRAFT` bit(1) NULL DEFAULT NULL,
  `NEEDREPLY` bit(1) NULL DEFAULT NULL,
  `MASS` bit(1) NULL DEFAULT NULL,
  `DOCID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RECEIVERUSERID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RECEIVERNAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_trigger
-- ----------------------------
DROP TABLE IF EXISTS `t_trigger`;
CREATE TABLE `t_trigger`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `TOKEN` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `JOB_TYPE` int(11) NULL DEFAULT NULL,
  `JOB_DATA` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `STATE` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DEADLINE` bigint(20) NULL DEFAULT NULL,
  `IS_LOOP` bit(1) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOCUMENTS` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `RUNTIMES` int(11) NULL DEFAULT NULL,
  `LASTMODIFIDATE` datetime(0) NULL DEFAULT NULL,
  `TASKID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_upload
-- ----------------------------
DROP TABLE IF EXISTS `t_upload`;
CREATE TABLE `t_upload`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `IMGBINARY` mediumblob NULL,
  `FIELDID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `TYPE` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FILESIZE` int(11) NULL DEFAULT NULL,
  `USERID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `MODIFYDATE` datetime(0) NULL DEFAULT NULL,
  `PATH` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `FOLDERPATH` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `SOURCEFILEID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `VERSIONNO` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOCID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RELATEMSG` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_workflow_reminder_history
-- ----------------------------
DROP TABLE IF EXISTS `t_workflow_reminder_history`;
CREATE TABLE `t_workflow_reminder_history`  (
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `REMINDER_CONTENT` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `USER_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `USER_NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NODE_NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOC_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `FLOW_INSTANCE_ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `PROCESS_TIME` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tlk_ocr
-- ----------------------------
DROP TABLE IF EXISTS `tlk_ocr`;
CREATE TABLE `tlk_ocr`  (
  `PARENT` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATE` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `INITIATOR` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `LASTFLOWOPERATION` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ITEM_图片` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ITEM_内容` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tlk_sms
-- ----------------------------
DROP TABLE IF EXISTS `tlk_sms`;
CREATE TABLE `tlk_sms`  (
  `PARENT` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LASTMODIFIED` datetime(0) NULL DEFAULT NULL,
  `FORMNAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATE` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITUSER` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITDATE` datetime(0) NULL DEFAULT NULL,
  `AUTHOR` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUTHORDEPTID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CREATED` datetime(0) NULL DEFAULT NULL,
  `FORMID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SUBFORMIDS` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `INITIATOR` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ISTMP` bit(1) NULL DEFAULT NULL,
  `VERSIONS` int(11) NULL DEFAULT NULL,
  `APPLICATIONID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `STATEINT` int(11) NULL DEFAULT NULL,
  `STATELABEL` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITORNAMES` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `LASTFLOWOPERATION` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LASTMODIFIER` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DOMAINID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AUDITORLIST` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `COAUDITORLIST` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `STATELABELINFO` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `PREVAUDITNODE` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `PREVAUDITUSER` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `OPTIONITEM` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `SIGN` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `KINGGRIDSIGNATURE` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ITEM_手机号` decimal(22, 10) NULL DEFAULT NULL,
  `ITEM_内容` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `ID` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
